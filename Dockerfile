FROM                    cpoepke/mountebank-basis:latest
 
COPY ./* /mb/
 
EXPOSE 2525
EXPOSE 9000
 
CMD mb --configfile /mb/imposters.ejs --allowInjection --loglevel debug
